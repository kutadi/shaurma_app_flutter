import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'package:JobFair/blocs/bloc_provider.dart';
import 'package:JobFair/blocs/favorite_bloc.dart';
import 'package:JobFair/blocs/favorite_item_bloc.dart';
import 'package:JobFair/models/fair_item.dart';
import 'package:JobFair/pages/details_page.dart';

class FairCardWidget extends StatefulWidget {
  final FairItem fairItem;
  final Stream<List<FairItem>> favoritesStream;

  FairCardWidget(this.fairItem, this.favoritesStream);

  @override
  _FairCardWidgetState createState() => _FairCardWidgetState();
}

class _FairCardWidgetState extends State<FairCardWidget> {
  FavoriteMovieBloc _bloc;

  ///
  /// In order to determine whether this particular Movie is
  /// part of the list of favorites, we need to inject the stream
  /// that gives us the list of all favorites to THIS instance
  /// of the BLoC
  ///
  StreamSubscription _subscription;

  @override
  void initState() {
    super.initState();
    _createBloc();
  }
  // @override
  // void didChangeDependencies() {
  //   super.didChangeDependencies();

  //   // As the context should not be used in the "initState()" method,
  //   // prefer using the "didChangeDependencies()" when you need
  //   // to refer to the context at initialization time
  //   _createBloc();
  // }

  ///
  /// As Widgets can be changed by the framework at any time,
  /// we need to make sure that if this happens, we keep on
  /// listening to the stream that notifies us about favorites
  ///
  @override
  void didUpdateWidget(FairCardWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    _disposeBloc();
    _createBloc();
  }

  @override
  void dispose() {
    _disposeBloc();
    super.dispose();
  }

  void _createBloc() {
    _bloc = FavoriteMovieBloc(widget.fairItem);

    // Simple pipe from the stream that lists all the favorites into
    // the BLoC that processes THIS particular movie
    _subscription = widget.favoritesStream.listen(_bloc.inFavorites.add);
  }

  void _disposeBloc() {
    _subscription.cancel();
    _bloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final FavoriteBloc bloc = BlocProvider.of<FavoriteBloc>(context);

    void _detailsPage() async {
      await Future.delayed(
        const Duration(milliseconds: 150),
        () => Navigator.push(
              context,
              CupertinoPageRoute(
                  builder: (context) => DetailsPage(widget.fairItem, bloc.outFavorites)),
            ),
      );
    }

    Widget companyPoster() {
      return Hero(
        tag: widget.fairItem,
        child: Padding(
          padding: EdgeInsets.only(top: 50.0),
          child: Material(
            elevation: 4.0,
            borderRadius: BorderRadius.circular(3.0),
            color: Colors.white,
            child: Ink.image(
              image: widget.fairItem.isUni == 'true'
                  ? AssetImage('assets/img/uni.png')
                  : AssetImage('assets/img/company.png'),
              fit: BoxFit.cover,
              width: 100.0,
              height: 100.0,
              child: InkWell(
                onTap: _detailsPage,
              ),
            ),
          ),
        ),
      );
    }

    Widget companyTicket() {
      return StreamBuilder(
        stream: _bloc.outIsFavorite,
        initialData: false,
        builder: (context, snapshot) {
          if (snapshot.data) {
            return Padding(
              padding: EdgeInsets.only(top: 150.0, right: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Container(
                    width: 55.0,
                    height: 55.0,
                    child: RaisedButton(
                      //shape: CircleBorder(),
                      color: Colors.lightBlue,
                      onPressed: () {
                        bloc.inRemoveFavorite.add(widget.fairItem);
                      },
                      child: Icon(
                        Icons.bookmark,
                        color: Colors.white,
                      ),
                    ),
                  )
                ],
              ),
            );
          } else {
            return Padding(
              padding: EdgeInsets.only(top: 150.0, right: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Container(
                    width: 55.0,
                    height: 55.0,
                    child: RaisedButton(
                      //shape: CircleBorder(),
                      color: Colors.lightBlue,
                      onPressed: () {
                        bloc.inAddFavorite.add(widget.fairItem);
                      },
                      child: Icon(
                        Icons.bookmark_border,
                        color: Colors.white,
                      ),
                    ),
                  )
                ],
              ),
            );
          }
        },
      );
    }

    Widget companyCard() {
      return Container(
        margin: EdgeInsets.only(left: 50.0, top: 16.0, bottom: 16.0),
        child: Card(
          child: InkWell(
            onTap: _detailsPage,
            child: Padding(
              padding: EdgeInsets.only(
                  left: 60.0, bottom: 8.0, top: 8.0, right: 8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                      widget.fairItem.name.length > 0
                          ? widget.fairItem.name
                          : widget.fairItem.fullname,
                      style: Theme.of(context).textTheme.subtitle),
                  Row(),
                ],
              ),
            ),
          ),
        ),
      );
    }

    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 16.0,
        vertical: 4.0,
      ),
      child: Container(
        height: 200.0,
        child: Stack(
          children: <Widget>[companyCard(), companyPoster(), companyTicket()],
        ),
      ),
    );
  }
}

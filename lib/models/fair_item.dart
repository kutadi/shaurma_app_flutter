import 'dart:convert';

FairItem fairItemFromJson(String str) {
  final jsonData = json.decode(str);
  return FairItem.fromJson(jsonData);
}

String fairItemItemToJson(FairItem data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}


List<FairItem> allCompaniesFromJson(String str) {
  final jsonData = json.decode(str);
  return List<FairItem>.from(jsonData.map((x) => FairItem.fromJson(x)));
}

String allCompaniesToJson(List<FairItem> data) {
  final dyn = List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class FairItem {
  String name;
  String email;
  String fullname;
  String phone;
  String address;
  String isUni;

  FairItem({
    this.name,
    this.email,
    this.fullname,
    this.phone,
    this.address,
    this.isUni = 'false'
  });

  factory FairItem.fromJson(Map<String, dynamic> json) => FairItem(
    name: json["name"],
    email: json["email"],
    fullname: json["fullName"],
    phone: json["phone"],
    address: json["address"]
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "email": email,
    "fullName": fullname,
    "phone": phone,
    "address": address
  };
}
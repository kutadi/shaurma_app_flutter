import 'package:flutter/material.dart';

import 'package:JobFair/blocs/bloc_provider.dart';
import 'package:JobFair/blocs/check_connection_bloc.dart';
import 'package:JobFair/blocs/company_catalog_bloc.dart';
import 'package:JobFair/blocs/favorite_bloc.dart';
import 'package:JobFair/models/fair_item.dart';
import 'package:JobFair/widgets/fair_item_card.dart';

import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:flare_flutter/flare_actor.dart';

class CompaniesPage extends StatefulWidget {
  @override
  CompaniesPageState createState() => CompaniesPageState();
}

class CompaniesPageState extends State<CompaniesPage>
    with AutomaticKeepAliveClientMixin<CompaniesPage> {
  CompanyCatalogBloc companyCatalogBloc;

  @override
  void initState() {
    super.initState();
    companyCatalogBloc = CompanyCatalogBloc();
  }

  @override
  void dispose() {
    companyCatalogBloc?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final CheckConnectionBloc checkConnectionBloc =
        BlocProvider.of<CheckConnectionBloc>(context);
    final FavoriteBloc favoriteBloc = BlocProvider.of<FavoriteBloc>(context);

    return BlocProvider(
      bloc: companyCatalogBloc,
      child: StreamBuilder<List<FairItem>>(
        stream: companyCatalogBloc.outCompanyList,
        builder: (builder, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.isNotEmpty) {
              return LiquidPullToRefresh(
                showChildOpacityTransition: false,
                onRefresh: companyCatalogBloc.handleRefresh,
                child: ListView.builder(
                  //key: PageStorageKey('companyList'),
                  itemCount: snapshot.data.length,
                  itemBuilder: (builder, index) => FairCardWidget(
                      snapshot.data[index], favoriteBloc.outFavorites),
                ),
              );
            } else {
              return StreamBuilder<bool>(
                stream: checkConnectionBloc.outConntection,
                builder: (builder, snapshot) {
                  if (snapshot.hasData) {
                    if (snapshot.data) {
                      companyCatalogBloc.handleRefresh();
                      return Center(child: CircularProgressIndicator());
                    } else {
                      return Column(
                        // mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            child: FlareActor(
                              'assets/anim/NoInternet.flr',
                              animation: 'idle',
                              alignment: Alignment.center,
                            ),
                          ),
                          Text('No Internet :(', textScaleFactor: 2,),
                          // RaisedButton(
                          //   child: Icon(Icons.update),
                          //   onPressed: () {
                          //     companyCatalogBloc.handleUpdateList();
                          //   },
                          // )
                        ],
                      );
                    }
                  } else {
                    return Container();
                  }
                },
              );
            }
          } else {
            return Container();
          }
        },
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

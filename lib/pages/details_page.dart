import 'dart:ui';
import 'dart:async';

import 'package:flutter/material.dart';

import 'package:JobFair/blocs/bloc_provider.dart';
import 'package:JobFair/blocs/favorite_bloc.dart';
import 'package:JobFair/blocs/favorite_item_bloc.dart';
import 'package:JobFair/models/fair_item.dart';

class DetailsPage extends StatefulWidget {
  final FairItem fairItem;
  final Stream<List<FairItem>> favoritesStream;

  DetailsPage(this.fairItem, this.favoritesStream);

  @override
  DetailsPageState createState() => DetailsPageState();
}

class DetailsPageState extends State<DetailsPage> {
  FavoriteMovieBloc _bloc;

  ///
  /// In order to determine whether this particular Movie is
  /// part of the list of favorites, we need to inject the stream
  /// that gives us the list of all favorites to THIS instance
  /// of the BLoC
  ///
  StreamSubscription _subscription;

  @override
  void initState() {
    super.initState();
    _createBloc();
  }
  // @override
  // void didChangeDependencies() {
  //   super.didChangeDependencies();

  //   // As the context should not be used in the "initState()" method,
  //   // prefer using the "didChangeDependencies()" when you need
  //   // to refer to the context at initialization time
  //   _createBloc();
  // }

  ///
  /// As Widgets can be changed by the framework at any time,
  /// we need to make sure that if this happens, we keep on
  /// listening to the stream that notifies us about favorites
  ///
  @override
  void didUpdateWidget(DetailsPage oldWidget) {
    super.didUpdateWidget(oldWidget);
    _disposeBloc();
    _createBloc();
  }

  @override
  void dispose() {
    _disposeBloc();
    super.dispose();
  }

  void _createBloc() {
    _bloc = FavoriteMovieBloc(widget.fairItem);

    // Simple pipe from the stream that lists all the favorites into
    // the BLoC that processes THIS particular movie
    _subscription = widget.favoritesStream.listen(_bloc.inFavorites.add);
  }

  void _disposeBloc() {
    _subscription.cancel();
    _bloc.dispose();
  }

  Container _getBackGround() {
    return Container(
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          widget.fairItem.isUni == 'true'
              ? Image.asset(
                  'assets/img/uni.png',
                  fit: BoxFit.cover,
                  height: 300.0,
                )
              : Image.asset(
                  'assets/img/company.png',
                  fit: BoxFit.cover,
                  height: 300.0,
                ),
          ClipRect(
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: Container(
                height: 300.0,
                decoration:
                    BoxDecoration(color: Colors.grey.shade200.withOpacity(0.5)),
              ),
            ),
          ),
        ],
      ),
      //constraints: BoxConstraints.expand(height: 300.0),
    );
  }

  Container _getGradient() {
    return Container(
      margin: EdgeInsets.only(top: 190.0),
      height: 110.0,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: <Color>[Color(0xfafafa), Colors.white],
              stops: [0.0, 0.9],
              begin: FractionalOffset(0.0, 0.0),
              end: FractionalOffset(0.0, 1.0))),
    );
  }

  Widget _getCard() {
    final itemLogo = Container(
      margin: EdgeInsets.symmetric(vertical: 16.0),
      alignment: FractionalOffset.center,
      child: Hero(
        tag: widget.fairItem,
        child: Material(
          elevation: 4.0,
          borderRadius: BorderRadius.circular(3.0),
          color: Colors.white,
          child: Ink.image(
            image: widget.fairItem.isUni == 'true'
                ? AssetImage('assets/img/uni.png')
                : AssetImage('assets/img/company.png'),
            fit: BoxFit.cover,
            width: 100.0,
            height: 100.0,
            child: InkWell(
              onTap: () {},
            ),
          ),
        ),
      ),
    );

    final itemContent = Container(
      margin: EdgeInsets.fromLTRB(16.0, 42.0, 16.0, 16.0),
      //constraints: BoxConstraints.expand(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(height: 16.0),
          Text(
            widget.fairItem.fullname,
            style: Theme.of(context).textTheme.headline,
            textAlign: TextAlign.center,
          ),
          Container(height: 16.0),
        ],
      ),
    );

    final itemCard = Container(
      child: itemContent,
      //height: 154.0,
      margin: EdgeInsets.only(top: 72.0),
      decoration: BoxDecoration(
          color: Colors.white,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(8.0),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.black12,
                blurRadius: 10.0,
                offset: Offset(0.0, 10.0))
          ]),
    );

    return Container(
      margin: EdgeInsets.symmetric(
        vertical: 16.0,
        horizontal: 24.0,
      ),
      child: Stack(
        children: <Widget>[
          itemCard,
          itemLogo,
        ],
      ),
    );
  }

  Widget _getContent() {
    return ListView(
      padding: EdgeInsets.fromLTRB(0.0, 72.0, 0.0, 32.0),
      children: <Widget>[
        _getCard(),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 32.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'НОМЕР',
                style: Theme.of(context).textTheme.title,
              ),
              Text(widget.fairItem.phone),
              Container(height: 16.0),
              Text(
                'КОНТАКТНОЕ ЛИЦО',
                style: Theme.of(context).textTheme.title,
              ),
              Text(widget.fairItem.email),
              Container(height: 16.0),
              Text(
                'АДРЕСС',
                style: Theme.of(context).textTheme.title,
              ),
              Text(widget.fairItem.address),
            ],
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final FavoriteBloc bloc = BlocProvider.of<FavoriteBloc>(context);

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(widget.fairItem.name),
      ),
      body: Container(
        //constraints: BoxConstraints.expand(),
        child: Stack(
          children: <Widget>[
            _getBackGround(),
            _getGradient(),
            _getContent(),
          ],
        ),
      ),
      floatingActionButton: StreamBuilder(
        stream: _bloc.outIsFavorite,
        initialData: false,
        builder: (context, snapshot) {
          if (snapshot.data) {
            return FloatingActionButton(
              child: Icon(Icons.bookmark),
              onPressed: () {
                bloc.inRemoveFavorite.add(widget.fairItem);
              },
            );
          } else {
            return FloatingActionButton(
              child: Icon(Icons.bookmark_border),
              onPressed: () {
                bloc.inAddFavorite.add(widget.fairItem);
              },
            );
          }
        },
      ),
    );
  }
}

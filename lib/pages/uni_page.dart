import 'package:flutter/material.dart';

import 'package:JobFair/blocs/bloc_provider.dart';
import 'package:JobFair/blocs/check_connection_bloc.dart';
import 'package:JobFair/blocs/uni_catalog_bloc.dart';
import 'package:JobFair/blocs/favorite_bloc.dart';
import 'package:JobFair/models/fair_item.dart';
import 'package:JobFair/widgets/fair_item_card.dart';

import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:flare_flutter/flare_actor.dart';

class UniPage extends StatefulWidget {
  @override
  UniPageState createState() => UniPageState();
}

class UniPageState extends State<UniPage>
    with AutomaticKeepAliveClientMixin<UniPage> {
  UniCatalogBloc uniCatalogBloc;

  @override
  void initState() {
    super.initState();
    uniCatalogBloc = UniCatalogBloc();
  }

  @override
  void dispose() {
    uniCatalogBloc?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final CheckConnectionBloc checkConnectionBloc =
        BlocProvider.of<CheckConnectionBloc>(context);
    final FavoriteBloc favoriteBloc = BlocProvider.of<FavoriteBloc>(context);

    return BlocProvider(
      bloc: uniCatalogBloc,
      child: StreamBuilder<List<FairItem>>(
        stream: uniCatalogBloc.outUniList,
        builder: (builder, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.isNotEmpty) {
              return LiquidPullToRefresh(
                showChildOpacityTransition: false,
                onRefresh: uniCatalogBloc.handleRefresh,
                child: ListView.builder(
                  //key: PageStorageKey('companyList'),
                  itemCount: snapshot.data.length,
                  itemBuilder: (builder, index) => FairCardWidget(
                      snapshot.data[index], favoriteBloc.outFavorites),
                ),
              );
            } else {
              return StreamBuilder<bool>(
                stream: checkConnectionBloc.outConntection,
                builder: (builder, snapshot) {
                  if (snapshot.hasData) {
                    if (snapshot.data) {
                      uniCatalogBloc.handleUpdateList();
                      return Center(child: CircularProgressIndicator());
                    } else {
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            child: FlareActor(
                              'assets/anim/NoInternet.flr',
                              animation: 'phone_sway',
                              alignment: Alignment.center,
                            ),
                          ),
                          Text('No Internet :(', textScaleFactor: 2,),
                          // RaisedButton(
                          //   child: Icon(Icons.update),
                          //   onPressed: () {
                          //     uniCatalogBloc.handleUpdateList();
                          //   },
                          // )
                        ],
                      );
                    }
                  } else {
                    return Container();
                  }
                },
              );
            }
          } else {
            return Container();
          }
        },
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

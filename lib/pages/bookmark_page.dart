import 'package:flutter/material.dart';

import 'package:JobFair/blocs/bloc_provider.dart';
import 'package:JobFair/blocs/favorite_bloc.dart';
import 'package:JobFair/models/fair_item.dart';
import 'package:JobFair/widgets/fair_item_card.dart';

class BookmarkPage extends StatefulWidget {
  @override
  BookmarkPageState createState() => BookmarkPageState();
}

class BookmarkPageState extends State<BookmarkPage> {
  @override
  Widget build(BuildContext context) {
    final FavoriteBloc favoriteBloc = BlocProvider.of<FavoriteBloc>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Закладки'),
      ),
      body: StreamBuilder<List<FairItem>>(
        stream: favoriteBloc.outFavorites,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (builder, index) => FairCardWidget(
                    snapshot.data[index], favoriteBloc.outFavorites));
          } else {
            return Container();
          }
        },
      ),
    );
  }
}

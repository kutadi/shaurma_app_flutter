import 'package:flutter/material.dart';

//import 'package:JobFair/widgets/shaurma_app_icons.dart';
import 'package:JobFair/pages/companies_page.dart';
import 'package:JobFair/pages/uni_page.dart';

import 'package:giffy_dialog/giffy_dialog.dart';
import 'package:url_launcher/url_launcher.dart';

class HomePage extends StatefulWidget {
  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Главная'),
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.info_outline),
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (_) => FlareGiffyDialog(
                            buttonOkColor: Colors.blue,
                            buttonOkText: Text(
                              'Перейти на сайт',
                              style: TextStyle(color: Colors.white),
                            ),
                            buttonCancelText: Text(
                              'Закрыть',
                              style: TextStyle(color: Colors.white),
                            ),
                            flarePath: 'assets/anim/onoi.logo.flr',
                            flareAnimation: 'logo',
                            title: Text(
                              'О приложении',
                              style: TextStyle(
                                  fontSize: 22.0, fontWeight: FontWeight.w600),
                            ),
                            description: Text(
                              'Данное приложение было разработано студентами INAI.KG для Портала практикантов и работодателей',
                              textAlign: TextAlign.center,
                              style: TextStyle(),
                            ),
                            onOkButtonPressed: () async {
                              const url = 'http://unibizz.inai.kg';
                              if (await canLaunch(url)) {
                                await launch(url);
                              } else {
                                throw 'Could not launch $url';
                              }
                            },
                          ));
                }),
          ],
          bottom: TabBar(tabs: [
            Tab(
              //icon: Icon(ShaurmaAppIcons.group),
              text: 'Компании',
            ),
            Tab(
                //icon: Icon(ShaurmaAppIcons.graduation_cap),
                text: 'Университеты'),
          ]),
        ),
        body: TabBarView(
          children: <Widget>[
            CompaniesPage(),
            UniPage(),
          ],
        ),
      ),
    );
  }
}

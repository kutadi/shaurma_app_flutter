import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// import 'package:fancy_bottom_navigation/fancy_bottom_navigation.dart';

import 'package:JobFair/blocs/bloc_provider.dart';
import 'package:JobFair/blocs/check_connection_bloc.dart';
import 'package:JobFair/blocs/favorite_bloc.dart';
import 'package:JobFair/pages/home_page.dart';
import 'package:JobFair/pages/bookmark_page.dart';

void main() async {
  await SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<CheckConnectionBloc>(
        bloc: CheckConnectionBloc(),
        child: BlocProvider(
          bloc: FavoriteBloc(),
          child: MaterialApp(
            title: 'INAI.KG: Приложение для практикантов',
            home: BottomNavigationDemo(),
          ),
        ));
  }
}

// class FancyBottomBar extends StatefulWidget {
//   @override
//   FancyBottomBarState createState() => FancyBottomBarState();
// }

// class FancyBottomBarState extends State<FancyBottomBar> {
//   Widget _currentPage = HomePage();
//   Widget _homePage = HomePage();
//   Widget _bookmarkPage = BookmarkPage();

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       bottomNavigationBar: FancyBottomNavigation(
//         tabs: [
//           TabData(iconData: Icons.home, title: "Home"),
//           TabData(iconData: Icons.bookmark, title: "Bookmarks"),
//         ],
//         onTabChangedListener: (position) {
//           setState(() {
//             _currentPage = position == 0 ? _homePage : _bookmarkPage;

//             // _currentPageText =
//             //     position == 0 ? Text('Home') : Text('Bookmarks');
//           });
//         },
//       ),
//       body: _currentPage,
//     );
//   }
// }

class NavigationIconView {
  NavigationIconView({
    Widget icon,
    Widget activeIcon,
    String title,
    Color color,
    TickerProvider vsync,
  })  : _icon = icon,
        _color = color,
        _title = title,
        item = BottomNavigationBarItem(
          icon: icon,
          activeIcon: activeIcon,
          title: Text(
            title,
            style: TextStyle(color: Colors.blue, fontWeight: FontWeight.w500),
          ),
          backgroundColor: color,
        ),
        controller = AnimationController(
          duration: kThemeAnimationDuration,
          vsync: vsync,
        ) {
    _animation = controller.drive(CurveTween(
      curve: const Interval(0.5, 1.0, curve: Curves.fastOutSlowIn),
    ));
  }

  final Widget _icon;
  final Color _color;
  final String _title;
  final BottomNavigationBarItem item;
  final AnimationController controller;
  Animation<double> _animation;

  FadeTransition transition(
      BottomNavigationBarType type, BuildContext context) {
    Color iconColor;
    if (type == BottomNavigationBarType.shifting) {
      iconColor = _color;
    } else {
      final ThemeData themeData = Theme.of(context);
      iconColor = themeData.brightness == Brightness.light
          ? themeData.primaryColor
          : themeData.accentColor;
    }

    return FadeTransition(
      opacity: _animation,
      child: SlideTransition(
        position: _animation.drive(
          Tween<Offset>(
            begin: const Offset(0.0, 0.02), // Slightly down.
            end: Offset.zero,
          ),
        ),
        child: IconTheme(
          data: IconThemeData(
            color: iconColor,
            size: 120.0,
          ),
          child: Semantics(
            label: 'Placeholder for $_title tab',
            child: _icon,
          ),
        ),
      ),
    );
  }
}

class BottomNavigationDemo extends StatefulWidget {
  @override
  _BottomNavigationDemoState createState() => _BottomNavigationDemoState();
}

class _BottomNavigationDemoState extends State<BottomNavigationDemo>
    with TickerProviderStateMixin {
  int _currentIndex = 0;
  BottomNavigationBarType _type = BottomNavigationBarType.shifting;
  List<NavigationIconView> _navigationViews;

  Widget _currentPage = HomePage();
  Widget _homePage = HomePage();
  Widget _bookmarkPage = BookmarkPage();

  @override
  void initState() {
    super.initState();
    _navigationViews = <NavigationIconView>[
      NavigationIconView(
        activeIcon: const Icon(Icons.home, color: Colors.blue),
        icon: const Icon(Icons.home, color: Colors.grey),
        title: 'Главная',
        // color: Colors.blue,
        vsync: this,
      ),
      NavigationIconView(
        activeIcon: const Icon(Icons.bookmark, color: Colors.blue),
        icon: const Icon(Icons.bookmark, color: Colors.grey),
        title: 'Закладки',
        // color: Colors.blue,
        vsync: this,
      ),
    ];

    _navigationViews[_currentIndex].controller.value = 1.0;
  }

  @override
  void dispose() {
    for (NavigationIconView view in _navigationViews) view.controller.dispose();
    super.dispose();
  }

  // Widget _buildTransitionsStack() {
  //   final List<FadeTransition> transitions = <FadeTransition>[];

  //   for (NavigationIconView view in _navigationViews)
  //     transitions.add(view.transition(_type, context));

  //   // We want to have the newly animating (fading in) views on top.
  //   transitions.sort((FadeTransition a, FadeTransition b) {
  //     final Animation<double> aAnimation = a.opacity;
  //     final Animation<double> bAnimation = b.opacity;
  //     final double aValue = aAnimation.value;
  //     final double bValue = bAnimation.value;
  //     return aValue.compareTo(bValue);
  //   });

  //   return Stack(children: transitions);
  // }

  @override
  Widget build(BuildContext context) {
    final BottomNavigationBar botNavBar = BottomNavigationBar(
      items: _navigationViews
          .map<BottomNavigationBarItem>(
              (NavigationIconView navigationView) => navigationView.item)
          .toList(),
      currentIndex: _currentIndex,
      type: _type,
      onTap: (int index) {
        setState(() {
          _navigationViews[_currentIndex].controller.reverse();
          _currentIndex = index;
          _navigationViews[_currentIndex].controller.forward();

          _currentPage = index == 0 ? _homePage : _bookmarkPage;
        });
      },
    );

    return Scaffold(
      body: _currentPage,
      bottomNavigationBar: botNavBar,
    );
  }
}

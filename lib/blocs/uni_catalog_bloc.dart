import 'dart:async';

import 'package:rxdart/rxdart.dart';

import 'package:JobFair/blocs/bloc_provider.dart';
import 'package:JobFair/data/database.dart';
import 'package:JobFair/data/api.dart';
import 'package:JobFair/models/fair_item.dart';

class UniCatalogBloc implements BlocBase {
  List<FairItem> _universities;

  PublishSubject<List<FairItem>> _uniController =
      PublishSubject<List<FairItem>>();
  Sink<List<FairItem>> get _inUniList => _uniController.sink;
  Stream<List<FairItem>> get outUniList => _uniController.stream;

  UniCatalogBloc() {
    _handleLoadList();
  }

  void dispose() {
    _uniController?.close();
  }

  void _handleLoadList() async {
    _universities = await DBProvider.db.getAllUni();
    _inUniList.add(List.unmodifiable(_universities));
  }

  void handleUpdateList() async {
    var list = await api.getAllUni();
    if(list.isNotEmpty) {
      await DBProvider.db.deleteAllUni();
      list.forEach((company) => DBProvider.db.newUni(company));
      _handleLoadList();
    }
  }

  Future<void> handleRefresh() {
    return api.getAllUni().then((list) async {
      if (list.isNotEmpty) {
        await DBProvider.db.deleteAllUni();
        list.forEach((company) => DBProvider.db.newUni(company));
        _handleLoadList();
      }
    });
  }
}

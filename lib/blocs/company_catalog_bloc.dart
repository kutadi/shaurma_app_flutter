import 'dart:async';

import 'package:rxdart/rxdart.dart';

import 'package:JobFair/blocs/bloc_provider.dart';
import 'package:JobFair/data/database.dart';
import 'package:JobFair/data/api.dart';
import 'package:JobFair/models/fair_item.dart';

class CompanyCatalogBloc implements BlocBase {
  List<FairItem> _companies;

  PublishSubject<List<FairItem>> _companiesController =
      PublishSubject<List<FairItem>>();
  Sink<List<FairItem>> get _inCompanyList => _companiesController.sink;
  Stream<List<FairItem>> get outCompanyList => _companiesController.stream;

  CompanyCatalogBloc() {
    _handleLoadList();
  }

  void dispose() {
    _companiesController?.close();
  }

  void _handleLoadList() async {
    _companies = await DBProvider.db.getAllCompanies();
    _inCompanyList.add(List.unmodifiable(_companies));
  }

  void handleUpdateList() async {
    var list = await api.getAllCompanies();
    if (list.isNotEmpty) {
      await DBProvider.db.deleteAllCompanies();
      list.forEach((company) => DBProvider.db.newCompany(company));
      _handleLoadList();
    }
  }

  Future<void> handleRefresh() {
    return api.getAllCompanies().then((list) async {
      if (list.isNotEmpty) {
        await DBProvider.db.deleteAllCompanies();
        list.forEach((company) => DBProvider.db.newCompany(company));
        _handleLoadList();
      }
    });
  }
}

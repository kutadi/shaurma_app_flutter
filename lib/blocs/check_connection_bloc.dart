import 'dart:io';
import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:rxdart/rxdart.dart';

import 'package:JobFair/blocs/bloc_provider.dart';

class CheckConnectionBloc extends BlocBase {
  bool hasConnection = false;

  BehaviorSubject<bool> _connectionController = BehaviorSubject<bool>();
  Sink<bool> get _inConnection => _connectionController.sink;
  Stream<bool> get outConntection => _connectionController.stream;

  final Connectivity _connectivity = Connectivity();

  CheckConnectionBloc() {
    _connectivity.onConnectivityChanged.listen(_handleGetConnection);

    _handleGetConnection(null);
  }

  void dispose() {
    _connectionController?.close();
  }

  void _handleGetConnection(data) async {
    await checkConnection();
  }

  Future<bool> checkConnection() async {
    // bool previousConnection = hasConnection;

    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        hasConnection = true;
      } else {
        hasConnection = false;
      }
    } on SocketException catch (_) {
      hasConnection = false;
    }

    //The connection status changed send out an update to all listeners
    // if (previousConnection != hasConnection) {
    // }
    _inConnection.add(hasConnection);

    return hasConnection;
  }
}

import 'dart:async';
import 'dart:collection';

import 'package:JobFair/data/database.dart';
import 'package:JobFair/blocs/bloc_provider.dart';
import 'package:JobFair/models/fair_item.dart';
import 'package:rxdart/rxdart.dart';


class FavoriteBloc implements BlocBase {

  ///
  /// Unique list of all favorite movies
  ///
  List<FairItem> _favorites = List<FairItem>();

  // ##########  STREAMS  ##############
  ///
  /// Interface that allows to add a new favorite movie
  ///
  BehaviorSubject<FairItem> _favoriteAddController = new BehaviorSubject<FairItem>();
  Sink<FairItem> get inAddFavorite => _favoriteAddController.sink;

  ///
  /// Interface that allows to remove a movie from the list of favorites
  ///
  BehaviorSubject<FairItem> _favoriteRemoveController = new BehaviorSubject<FairItem>();
  Sink<FairItem> get inRemoveFavorite => _favoriteRemoveController.sink;

  ///
  /// Interface that allows to get the list of all favorite movies
  ///
  BehaviorSubject<List<FairItem>> _favoritesController = new BehaviorSubject<List<FairItem>>(seedValue: []);
  Sink<List<FairItem>> get _inFavorites =>_favoritesController.sink;
  Stream<List<FairItem>> get outFavorites =>_favoritesController.stream;

  ///
  /// Constructor
  ///
  FavoriteBloc(){
    _favoriteAddController.listen(_handleAddFavorite);
    _favoriteRemoveController.listen(_handleRemoveFavorite);

    _loadData();
  }

  void dispose(){
    _favoriteAddController.close();
    _favoriteRemoveController.close();
    _favoritesController.close();
  }

  // ############# HANDLING  #####################

  void _handleAddFavorite(FairItem fairItem) async {
    // Add the movie to the list of favorite ones
    await DBProvider.db.newBookmark(fairItem);

    _favorites.add(fairItem);

    _notify();
  }

  void _handleRemoveFavorite(FairItem fairItem) async {
    await DBProvider.db.deleteBookmark(fairItem);

    _favorites.remove(fairItem);

    _notify();
  }

  void _notify(){
    // The new list of all favorite movies
    _inFavorites.add(UnmodifiableListView(_favorites));
  }

  void _loadData() async {
    _favorites = await DBProvider.db.getAllBookmarks();
    _notify();
  }
}

import 'dart:async';

import 'package:JobFair/blocs/bloc_provider.dart';
import 'package:JobFair/models/fair_item.dart';
import 'package:rxdart/rxdart.dart';

class FavoriteMovieBloc implements BlocBase {
  ///
  /// A stream only meant to return whether THIS movie is part of the favorites
  ///
  final BehaviorSubject<bool> _isFavoriteController = BehaviorSubject<bool>();
  Stream<bool> get outIsFavorite => _isFavoriteController.stream;

  ///
  /// Stream of all the favorites
  ///
  final StreamController<List<FairItem>> _favoritesController = StreamController<List<FairItem>>();
  Sink<List<FairItem>> get inFavorites => _favoritesController.sink;

  ///
  /// Constructor
  ///
  FavoriteMovieBloc(FairItem fairItem){
    //
    // We are listening to all favorites
    //
    _favoritesController.stream
                        // but, we only consider the one that matches THIS one
                        .map((list) => list.any((FairItem item) => item.name == fairItem.name))
                        // if any, we notify that it is part of the Favorites
                        .listen((isFavorite) => _isFavoriteController.add(isFavorite));
  }

  void dispose(){
    _favoritesController.close();
    _isFavoriteController.close();
  }
}
import 'dart:async';
import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:JobFair/models/fair_item.dart';

class DBProvider {
  DBProvider._();

  static final DBProvider db = DBProvider._();

  Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "shaurma.db");
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE Company ("
          "name TEXT PRIMARY KEY,"
          "email TEXT,"
          "fullName TEXT,"
          "phone TEXT, "
          "address TEXT"
          ")");

      await db.execute("CREATE TABLE University ("
          "name TEXT PRIMARY KEY,"
          "email TEXT,"
          "fullName TEXT,"
          "phone TEXT, "
          "address TEXT"
          ")");

      await db.execute("CREATE TABLE Bookmark ("
          "name TEXT PRIMARY KEY,"
          "email TEXT,"
          "fullName TEXT,"
          "phone TEXT, "
          "address TEXT, "
          "uni TEXT"
          ")");
    });
  }

  newCompany(FairItem newCompany) async {
    final db = await database;
    //insert to the table
    var raw = await db.rawInsert(
        "INSERT Into Company (name,email,fullName,phone,address)"
        " VALUES (?,?,?,?,?)",
        [
          newCompany.name,
          newCompany.email,
          newCompany.fullname,
          newCompany.phone,
          newCompany.address
        ]);
    return raw;
  }

  newUni(FairItem newUni) async {
    final db = await database;
    //insert to the table
    var raw = await db.rawInsert(
        "INSERT Into University (name,email,fullName,phone,address)"
        " VALUES (?,?,?,?,?)",
        [
          newUni.name,
          newUni.email,
          newUni.fullname,
          newUni.phone,
          newUni.address
        ]);
    return raw;
  }

  newBookmark(FairItem newBookmark) async {
    final db = await database;
    //insert to the table
    var raw = await db.rawInsert(
        "INSERT Into Bookmark (name,email,fullName,phone,address,uni)"
        " VALUES (?,?,?,?,?,?)",
        [
          newBookmark.name,
          newBookmark.email,
          newBookmark.fullname,
          newBookmark.phone,
          newBookmark.address,
          newBookmark.isUni
        ]);
    return raw;
  }

  Future<List<FairItem>> getAllCompanies() async {
    final db = await database;
    var res = await db.query("Company");
    List<FairItem> list =
        res.isNotEmpty ? res.map((c) => FairItem.fromJson(c)).toList() : [];
    return list;
  }

  Future<List<FairItem>> getAllUni() async {
    final db = await database;
    var res = await db.query("University");
    List<FairItem> list =
        res.isNotEmpty ? res.map((c) => FairItem.fromJson(c)).toList() : [];

    list.forEach((item) => item.isUni = 'true');

    return list;
  }

  Future<List<FairItem>> getAllBookmarks() async {
    final db = await database;
    var res = await db.query("Bookmark", where: 'uni = ?', whereArgs: ['true']);
    List<FairItem> list =
        res.isNotEmpty ? res.map((c) => FairItem.fromJson(c)).toList() : [];

    list.forEach((item) => item.isUni = 'true');

    res = await db.query("Bookmark", where: 'uni = ?', whereArgs: ['false']);
    list.addAll(res.isNotEmpty? res.map((c) => FairItem.fromJson(c)).toList() : []);

    return list;
  }

  deleteBookmark(FairItem bookmark) async {
    final db = await database;
    return db
        .delete("Bookmark", where: "email = ?", whereArgs: [bookmark.email]);
  }

  deleteAllBookmarks() async {
    final db = await database;
    db.rawDelete("Delete from Bookmark");
  }

  deleteAllCompanies() async {
    final db = await database;
    db.rawDelete("Delete from Company");
  }

  deleteAllUni() async {
    final db = await database;
    db.rawDelete("Delete from University");
  }
}

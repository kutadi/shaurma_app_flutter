import 'dart:async';
import 'package:http/http.dart' as http;

import 'package:JobFair/models/fair_item.dart';

class Api {
  static const String url = 'http://unibizz.inai.kg/company?is_university=';

  Future<List<FairItem>> getAllCompanies() async {
    final response = await http.get(url + 'false');
    return allCompaniesFromJson(response.body);
  }

  Future<List<FairItem>> getAllUni() async {
    final response = await http.get(url + 'true');
    return allCompaniesFromJson(response.body);
  }
}

Api api = Api();
